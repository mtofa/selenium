﻿using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace SeleniumTest.MeetupTests
{
    public  class HomePage
    {
        public static string Url = "http://localhost:56096";
        public static string PageTitle = "Home Page - My ASP.NET Application";

        //[FindsBy(How = How.LinkText, Using = "Log in")]
        //private IWebElement LoginLink;

        //[FindsBy(How = How.ClassName,Using = "input-email-1")]
        //private IWebElement OrganizerUsername;
        
        
       
        

        public  void Goto()
        {
            Browser.Goto(Url);
        }

        public void Maximize()
        {
            Browser.Driver.Manage().Window.Maximize();
        }

        public  bool IsAt()
        {
            return
                Browser.Title == PageTitle;
        }

        public void ClickLogin()
        {
            Browser.ClickLink("Log in");

        }

        public void ClickLogOff()
        {
            Browser.ClickClass("fa-user-circle-o");
            Browser.ClickLink("Log off");
        }

        public void SelectAMeetup()
        {
            Browser.ClickClass("fa-user-circle-o");
            Browser.ClickLink("Manage Groups");
            Browser.ClickLink("Melbourne Functional User Group (MFUG)");
        }

        public void AddaMeetup()
        {
            Browser.ClickLink("Add a Meetup");
            Browser.FillTextByName("Title", "Event 1");
            Browser.FillTextByName("Location", "Location 1");
            Browser.FillTextByName("Details", "Details 1");
            Browser.ClickClass("fa-calendar");
            Thread.Sleep(2000);
            // Browser.ClickXpath("//div[@class='datetimepicker-days']/table/thead/tr[1]/th[3]/i");
            Browser.ClickXpath("//div[@class='datetimepicker-days']/table/tbody/tr/td[contains(.,'20')]");
            Browser.ClickXpath("//div[@class='datetimepicker-hours']/table/tbody/tr/td/fieldset[2]/span[contains(.,'6')]");
            Browser.ClickXpath("//div[@class='datetimepicker-minutes']/table/tbody/tr/td/fieldset/span[contains(.,'6:30')]");
            Browser.ClickXpath("//button[@type='submit']");
        }

        

        public void EnterCredentials(string email, string password)
        {
            Browser.FillTextByClass("input-email-1",email );
            Browser.FillTextByClass("input-password-1",password );
            Browser.ClickClass("submit-1");
        }

        public void JoinAMeetup()
        {
            Browser.ClickLink("Event 1");
            Browser.ClickName("singlebutton2");
        }

        public void CancleaMeetup()
        {
            Browser.ClickClass("fa-remove");
            Browser.ClickClass("confirm-button");
        }

        public void ReadNotification()
        {
            Thread.Sleep(100);
            Browser.ClickClass("badge");
            Thread.Sleep(100);
            Browser.ClickXpath("//div[@class='popover-content']/ul/li/div/a");
            string actualvalue = Browser.Driver.FindElement(By.ClassName("groupView-left")).Text;
            Assert.IsTrue(actualvalue.Contains("Event 1"), actualvalue + " doesn't contains 'Event 1'");
          
        }
    }
}